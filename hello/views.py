from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm
import datetime

# Create your views here.
def index(request):
    if request.method == "POST":
        form = StatusForm(request.POST or None)
        if form.is_valid():
            status = request.POST['status']
            post = Status(status = status, date = datetime.date.today)
            post.save()
            return redirect('/')
    else:
        form = StatusForm()
    return render(request, 'hello.html', {'form' : form, 'listStatus' : Status.objects.all()})  