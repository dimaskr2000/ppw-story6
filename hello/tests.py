from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from .forms import StatusForm
from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.
class HelloTest(TestCase):

    def test_page_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_index_func(self):
        function = resolve('/')
        self.assertEqual(function.func, index)

    def test_hom_contains_hello(self):
        response = Client().get('')
        self.assertContains(response, 'Hello, apa kabar?')
    
    def test_model_can_create_status(self):
        new_status = Status.objects.create(status="Test")
        count_all_variable = Status.objects.all().count()
        self.assertEqual(count_all_variable, 1)

    def test_form_validation(self):
        form = StatusForm(data={'status':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'],['This field is required.'])

    def test_homepage_post_success_and_render_the_result(self):
        test = 'Test'
        response_post = Client().post('/', {'status': test})
        self.assertEqual(response_post.status_code, 302)
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_homepage_post_failed_and_render_the_result(self):
        test = 'Test'
        response_post = Client().post('/', {'status': ''})
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

class FunctionalTests(unittest.TestCase):

    def setUp(self):
        self.main_link = "http://localhost:8000/"
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        self.browser = webdriver.Chrome("chromedriver", chrome_options=options)

    def tearDown(self):
        self.browser.quit()

    def test_new_status_is_saved_and_posted(self):
        self.browser.get(self.main_link)
        status_message = "Coba Coba"
        status_input = self.browser.find_elements_by_xpath("//*[@id='id_status']")[0]
        status_input.send_keys(status_message)
        status_input.submit()
        self.browser.get(self.main_link)
        self.assertIn(status_message, self.browser.page_source)

    def test_hello_container_is_on_the_top(self):
        self.browser.get(self.main_link)
        self.assertTrue(self.browser.find_elements_by_css_selector("body > header")[0].is_displayed())

    def test_input_status_after_hello_container(self):
        self.browser.get(self.main_link)
        self.assertTrue(self.browser.find_elements_by_css_selector("body > div.ui")[0].is_displayed())

    def test_hello_background_color(self):
        self.browser.get(self.main_link)
        background = self.browser.find_elements_by_xpath("/html/body/header/h1")[0]
        self.assertEqual(background.value_of_css_property("background-color"), "rgba(0, 0, 0, 0)")

    def test_layout_must_be_block(self):
        self.browser.get(self.main_link)
        background = self.browser.find_elements_by_xpath("/html/body/div")[0]
        self.assertEqual(background.value_of_css_property("display"), "block")


