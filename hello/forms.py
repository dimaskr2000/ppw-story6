from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    status = forms.CharField(widget = forms.TextInput(attrs = {'type':'text','placeholder':"What's on your mind?"}), required = True)
    
    class Meta:
        model = Status
        fields = ('status',)
